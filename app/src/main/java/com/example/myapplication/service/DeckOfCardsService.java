package com.example.myapplication.service;

import com.example.myapplication.model.DrawCardsResult;
import com.example.myapplication.model.ShuffleResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DeckOfCardsService {
    public final String BASE_URL = "https://deckofcardsapi.com/api/deck/";

    @GET("new/shuffle/")
    Call<ShuffleResult> shuffleDecks(@Query("deck_count") int deckCount);

    @GET("{deck_id}/draw/")
    Call<DrawCardsResult> drawCards(@Path("deck_id") String deckId, @Query("count") int count);

    @GET("{deck_id}/shuffle/")
    Call<ShuffleResult> reshuffleDeck(@Path("deck_id") String deckId);
}
