package com.example.myapplication.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.Constants;
import com.example.myapplication.adapter.GridViewAdapter;
import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.gameengine.GameEngine;
import com.example.myapplication.model.DrawCardsResult;
import com.example.myapplication.model.ShuffleResult;
import com.example.myapplication.service.DeckOfCardsService;
import com.example.myapplication.util.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GameFragment extends Fragment {
    private static final String TAG = GameFragment.class.getSimpleName();

    private Integer numOfDecks;
    private String deckId;
    private List<DrawCardsResult.Card> drawnCards;
    private boolean lastChance = false;

    private Button drawButton, shuffleButton;
    private GridView gridView;
    private GridViewAdapter gridViewAdapter;
    private ProgressBar progressBar;
    private TextView startHintTextView;

    private DeckOfCardsService deckOfCardsService;
    private GameEngine gameEngine;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_game, container, false);
//        Log.v(TAG, "numOfDecks: " + ((MainActivity) getActivity()).getNumOfDecks());
        numOfDecks = ((MainActivity) getActivity()).getNumOfDecks();

        deckOfCardsService = ((MainActivity) getActivity()).getDeckOfCardsService();
        gameEngine = GameEngine.getInstance();

        gridView = view.findViewById(R.id.grid_view);
        shuffleButton = view.findViewById(R.id.shuffle_deck_buitton);
        drawButton = view.findViewById(R.id.draw_cards_button);
        progressBar = view.findViewById(R.id.progress_bar);
        startHintTextView = view.findViewById(R.id.start_hint_text_view);

        shuffleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reshuffleCurrentDeck();
            }
        });
        drawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawCards();
            }
        });

        shuffleDecks();

        return view;
    }

    private void reshuffleCurrentDeck() {
        Log.v(TAG, "Reshuffle current deck");
        Call<ShuffleResult> reshuffleCall = deckOfCardsService.reshuffleDeck(deckId);
        reshuffleCall.enqueue(new Callback<ShuffleResult>() {
            @Override
            public void onResponse(Call<ShuffleResult> call, Response<ShuffleResult> response) {
                ShuffleResult shuffleResult = response.body();
                Log.v(TAG, "Reshuffled current deck, remaining card count: " + shuffleResult.getRemaining());
                gridView.setAdapter(null);
                Utils.displayToastAtCenterOfTheScreen(getContext(), getString(R.string.reshuffled_cards));
                startHintTextView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<ShuffleResult> call, Throwable t) {

            }
        });
    }

    private void shuffleDecks() {
        Log.v(TAG, "Shuffle decks");
        Call<ShuffleResult> shuffleCall = deckOfCardsService.shuffleDecks(numOfDecks);
        shuffleCall.enqueue(new Callback<ShuffleResult>() {
            @Override
            public void onResponse(Call<ShuffleResult> call, Response<ShuffleResult> response) {
                Log.v(TAG, "ShuffleResult: " + response.body());
                ShuffleResult shuffleResult = response.body();
                deckId = shuffleResult.getDeck_id();
                Log.v(TAG, "Shuffled decks, num of cards: " + shuffleResult.getRemaining());
            }

            @Override
            public void onFailure(Call<ShuffleResult> call, Throwable t) {

            }
        });
    }

    private void drawCards() {
        Log.v(TAG, "Draw cards");
        progressBar.setVisibility(View.VISIBLE);
        startHintTextView.setVisibility(View.GONE);
        Call<DrawCardsResult> drawCardsResultCall = deckOfCardsService.drawCards(deckId, Constants.NUM_OF_CARDS_TO_DRAW);
        drawCardsResultCall.enqueue(new Callback<DrawCardsResult>() {
            @Override
            public void onResponse(Call<DrawCardsResult> call, Response<DrawCardsResult> response) {
                DrawCardsResult drawCardsResult = response.body();
                Log.v(TAG, "Drawn cards, remaining: " + drawCardsResult.getRemaining());
                if (drawCardsResult.getRemaining() < Constants.NUM_OF_CARDS_TO_DRAW) {
                    lastChance = true;
                }
                drawnCards = drawCardsResult.getCards();
                displayCards(drawnCards);
            }

            @Override
            public void onFailure(Call<DrawCardsResult> call, Throwable t) {

            }
        });
    }



    private void displayCards(List<DrawCardsResult.Card> drawnCards) {
        List<String> urls = new ArrayList<>();
        for (DrawCardsResult.Card drawnCard : drawnCards) {
            urls.add(drawnCard.getImage());
        }
        gridViewAdapter = new GridViewAdapter(getContext(), urls);
        gridView.setAdapter(gridViewAdapter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(GridViewAdapter.CardsLoadingFinishedEvent event) throws InterruptedException {
        progressBar.setVisibility(View.GONE);
        String winInfo  = gameEngine.checkIfWon(getContext(), drawnCards);
        if (winInfo != null) {
            displayGameOverDialog(getString(R.string.victory), winInfo);
        } else if (lastChance == true) {
            displayGameOverDialog(getString(R.string.defeat), getString(R.string.defeat_message));
        }
    }

    private void displayGameOverDialog(String title, String message) {
        new AlertDialog.Builder(getContext()).setTitle(title).setMessage(message)
                .setPositiveButton(R.string.exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finishAffinity();
                    }
                }).setNegativeButton(R.string.play_again, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = getActivity().getIntent();
                getActivity().finish();
                startActivity(intent);
            }
        }).setCancelable(false).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
