package com.example.myapplication.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;


public class StartFragment extends Fragment {
    private static final String TAG = StartFragment.class.getSimpleName();

    private EditText numOfDecksEditText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start, container, false);

        numOfDecksEditText = view.findViewById(R.id.num_of_decks_edit_text);
        numOfDecksEditText.addTextChangedListener(new TextWatcher() {
            String beforeText;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                beforeText = s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() != 0) {
                    Integer number = Integer.parseInt(s.toString());
                    if (number > 5 || number < 1) {
                        numOfDecksEditText.setText(beforeText);
                    }
                }
                beforeText = null;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        Button startButton = view.findViewById(R.id.start_game_button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numOfDecksString = numOfDecksEditText.getText().toString();
                if (numOfDecksString.isEmpty()) {
                    Toast.makeText(getContext(), R.string.choose_num_of_decks, Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        Integer numOfDecks = Integer.valueOf(numOfDecksString);
                        ((MainActivity) getActivity()).startGame(numOfDecks);
                    } catch (NumberFormatException exception) {

                    }

                }
            }
        });

//        insertTestNumOfDecks();

        return view;
    }

    private void insertTestNumOfDecks() {
        numOfDecksEditText.setText("1");
    }


}
