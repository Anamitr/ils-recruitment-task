package com.example.myapplication.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class GridViewAdapter extends BaseAdapter {
    private final Context context;
    private final List<String> urls = new ArrayList<String>();

    public GridViewAdapter(Context context, List<String> urls) {
        this.context = context;
        this.urls.addAll(urls);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView view = (ImageView) convertView;
        if (view == null) {
            view = new ImageView(context);
        }

        String url = getItem(position);

        if (position == urls.size() - 1) {
            Picasso.get().load(url).into(view, new Callback() {
                @Override
                public void onSuccess() {
                    EventBus.getDefault().post(new CardsLoadingFinishedEvent());
                }

                @Override
                public void onError(Exception e) {

                }
            });
        } else {
            Picasso.get().load(url).into(view);
        }


        return view;
    }

    @Override
    public int getCount() {
        return urls.size();
    }

    @Override
    public String getItem(int position) {
        return urls.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class CardsLoadingFinishedEvent {  }
}
