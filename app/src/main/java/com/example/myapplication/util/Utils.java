package com.example.myapplication.util;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

import com.example.myapplication.R;

public class Utils {
    public static void displayToastAtCenterOfTheScreen(Context context, String message) {
        displayToastAtCenterOfTheScreen(context, message, Toast.LENGTH_SHORT);
    }

    public static void displayToastAtCenterOfTheScreen(Context context, String message, Integer duration) {
        Toast toast = Toast.makeText(context, message, duration);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
