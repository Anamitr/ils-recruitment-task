package com.example.myapplication.model;

public class ShuffleResult {
    private boolean success;
    private String deck_id;
    private boolean shuffled;
    private int remaining;


    // Getter Methods

    public boolean getSuccess() {
        return success;
    }

    public String getDeck_id() {
        return deck_id;
    }

    public boolean getShuffled() {
        return shuffled;
    }

    public int getRemaining() {
        return remaining;
    }

    // Setter Methods

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setDeck_id(String deck_id) {
        this.deck_id = deck_id;
    }

    public void setShuffled(boolean shuffled) {
        this.shuffled = shuffled;
    }

    public void setRemaining(int remaining) {
        this.remaining = remaining;
    }

    @Override
    public String toString() {
        return "ShuffleResult{" +
                "success=" + success +
                ", deck_id='" + deck_id + '\'' +
                ", shuffled=" + shuffled +
                ", remaining=" + remaining +
                '}';
    }
}
