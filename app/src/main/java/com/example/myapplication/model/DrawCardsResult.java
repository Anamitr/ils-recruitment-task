package com.example.myapplication.model;

import java.util.ArrayList;

public class DrawCardsResult {
    private boolean success;
    ArrayList<Card> cards = new ArrayList<Card>();
    private String deck_id;
    private int remaining;


    // Getter Methods
    public boolean getSuccess() {
        return success;
    }

    public String getDeck_id() {
        return deck_id;
    }

    public int getRemaining() {
        return remaining;
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    // Setter Methods

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setDeck_id(String deck_id) {
        this.deck_id = deck_id;
    }

    public void setRemaining(int remaining) {
        this.remaining = remaining;
    }

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
    }

    public class Card {
        private String image;
        private String value;
        private String suit;
        private String code;


        // Getter Methods

        public String getImage() {
            return image;
        }

        public String getValue() {
            return value;
        }

        public String getSuit() {
            return suit;
        }

        public String getCode() {
            return code;
        }

        // Setter Methods

        public void setImage(String image) {
            this.image = image;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public void setSuit(String suit) {
            this.suit = suit;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}
