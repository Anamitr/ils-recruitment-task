package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.myapplication.fragment.GameFragment;
import com.example.myapplication.fragment.StartFragment;
import com.example.myapplication.service.DeckOfCardsService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private Integer numOfDecks;
    private Retrofit retrofit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        retrofit = new Retrofit.Builder().baseUrl(DeckOfCardsService.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();

        StartFragment startFragment = new StartFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.dynamic_fragment_frame_layout, startFragment);
        ft.addToBackStack(startFragment.getClass().getName());
        ft.commit();
    }

    public void startGame(Integer numOfDecks) {
        this.numOfDecks = numOfDecks;
        Fragment gameFragment = new GameFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.dynamic_fragment_frame_layout, gameFragment);
        ft.addToBackStack(gameFragment.getClass().getName());
        ft.commit();
    }

    public DeckOfCardsService getDeckOfCardsService() {
        return  retrofit.create(DeckOfCardsService.class);
    }

    public Integer getNumOfDecks() {
        return numOfDecks;
    }

    public void setNumOfDecks(Integer numOfDecks) {
        this.numOfDecks = numOfDecks;
    }

}
