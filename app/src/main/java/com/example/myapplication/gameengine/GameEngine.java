package com.example.myapplication.gameengine;

import android.content.Context;
import android.util.Log;

import com.example.myapplication.R;
import com.example.myapplication.model.DrawCardsResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameEngine {
    private static final String TAG = GameEngine.class.getSimpleName();

    private static GameEngine instance = null;

    public static final Map<String, Integer> cardValuesMap = new HashMap<>();

    static {
        cardValuesMap.put("A", 1);
        cardValuesMap.put("0", 10);
        cardValuesMap.put("J", 11);
        cardValuesMap.put("Q", 12);
        cardValuesMap.put("K", 13);
    }

    List<DrawCardsResult.Card> cards;
    List<String> codes;

    List<Integer> values;
    List<String> colors;


    private GameEngine() {
    }

    public String checkIfWon(Context context, List<DrawCardsResult.Card> cards) {
        this.cards = cards;
        loadCardCodes(cards);
        loadCardValuesAndColors();

        if (checkIfTriplets()) {
            return context.getString(R.string.win_triplet_values);
        } else if (checkIfStairs()) {
            return context.getString(R.string.win_incremental_values);
        } else if (checkIfThreeFigures()) {
            return context.getString(R.string.win_three_figures);
        } else if (checkIfThreeSameColors()) {
            return context.getString(R.string.win_three_same_colors);
        }

        this.codes = null;
        this.cards = null;
        this.values = null;
        this.colors = null;
        return null;
    }

    private void loadCardCodes(List<DrawCardsResult.Card> cards) {
        codes = new ArrayList<>();
        for (DrawCardsResult.Card card : cards) {
            codes.add(card.getCode());
        }
    }

    private void loadCardValuesAndColors() {
        colors = new ArrayList<>();
        values = new ArrayList<>();
        for (String code : codes) {
            colors.add(code.split("")[2]);
            String cardValue = code.split("")[1];
            if (cardValuesMap.containsKey(cardValue)) {
                values.add(cardValuesMap.get(cardValue));
            } else {
                values.add(Integer.parseInt(cardValue));
            }
        }
        Log.v(TAG, "Values: " + values);
        Log.v(TAG, "Colors: " + colors);
    }

    private boolean checkIfTriplets() {
//        values = new ArrayList<>(Arrays.asList(3,7,11,7,3));
        boolean result = false;
        Map<Integer, Integer> duplicatesCountMap = new HashMap<>();
        for (Integer value : values) {
            if (duplicatesCountMap.containsKey(value)) {
                duplicatesCountMap.put(value, duplicatesCountMap.get(value) + 1);
            } else {
                duplicatesCountMap.put(value, 1);
            }
        }
        if (duplicatesCountMap.containsValue(3)) {
            result = true;
        }
        Log.v(TAG, "checkIfTriplets result: " + result);
        return result;
    }

    private boolean checkIfStairs() {
//        values = new ArrayList<>(Arrays.asList(3,4,5,7,3));
        boolean result = false;

        for (Integer value : values) {
            if ((values.contains(value - 1) && values.contains(value - 2))
                    || (values.contains(value + 1) && values.contains(value + 2))) {
                result = true;
            }
        }
        Log.v(TAG, "checkIfStairs result: " + result);
        return result;
    }

    private boolean checkIfThreeFigures() {
        boolean result = false;
        int figuresCount = 0;
        for (Integer value : values) {
            if (value > 10) {
                figuresCount++;
            }
        }
        if (figuresCount >= 3) {
            result = true;
        }
        Log.v(TAG, "checkIfThreeFigures result: " + result);
        return result;
    }

    private boolean checkIfThreeSameColors() {
        boolean result = false;
        Map<String, Integer> duplicatesColorMap = new HashMap<>();
        for (String color : colors) {
            if (duplicatesColorMap.containsKey(color)) {
                duplicatesColorMap.put(color, duplicatesColorMap.get(color) + 1);
                if (duplicatesColorMap.get(color) == 3) {
                    result = true;
                }
            } else {
                duplicatesColorMap.put(color, 1);
            }
        }
        Log.v(TAG, "checkIfTriplets result: " + result);

        return result;
    }

    public String getWinInfo() {
        return "TODO";
    }


    public static GameEngine getInstance() {
        if (instance == null) {
            instance = new GameEngine();
        }
        return instance;
    }
}
